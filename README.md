# mta-hosting-optimizer


### Steps To run: 
- In root of project use ``go run main.go`` command
- Then in terminal use curl command ``curl --location --request GET 'http://localhost:8000/inactive-hosts'``

#### Linters: 
`` golangci-lint run``
no linters

#### UT & Code Coverage:
```
go test -v -coverpkg=./... -coverprofile=cover.cov ./...
go tool cover -func=cover.cov && go tool cover -html=cover.cov 

github.com/harshit-dhawan/mta-hosting-optimizer/handlers/handler.go:17:         New                     100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/handlers/handler.go:21:         InactiveHost            100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/main.go:16:                     main                    73.3%
github.com/harshit-dhawan/mta-hosting-optimizer/main.go:41:                     GetEnvOrDefault         100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/main.go:50:                     NewGinApp               66.7%
github.com/harshit-dhawan/mta-hosting-optimizer/mockipconfig/ipconfig.go:8:     New                     100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/mockipconfig/ipconfig.go:12:    GetIPMockData           100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/mock_interfaces.go:27: NewMockHostStatus       100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/mock_interfaces.go:34: EXPECT                  100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/mock_interfaces.go:39: InactiveHosts           100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/mock_interfaces.go:48: InactiveHosts           100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/mock_interfaces.go:65: NewMockMockIPConfigSvc  100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/mock_interfaces.go:72: EXPECT                  100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/mock_interfaces.go:77: GetIPMockData           100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/mock_interfaces.go:85: GetIPMockData           100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/service.go:17:         New                     100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/service.go:21:         InactiveHosts           100.0%
github.com/harshit-dhawan/mta-hosting-optimizer/services/service.go:32:         processConfigData       100.0%
total:                                                                          (statements)            91.9%

```