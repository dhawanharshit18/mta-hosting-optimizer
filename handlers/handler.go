package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/harshit-dhawan/mta-hosting-optimizer/models"
	"github.com/harshit-dhawan/mta-hosting-optimizer/services"
)

type handler struct {
	svc services.HostStatus
}

//nolint:revive //handler has to be unexported
func New(s services.HostStatus) handler {
	return handler{svc: s}
}

func (h handler) InactiveHost(ctx *gin.Context) {
	inactiveHost, err := h.svc.InactiveHosts(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"Error": err.Error()})
		return
	}

	if val, ok := inactiveHost.([]string); ok {
		if len(val) == 0 && err == nil {
			ctx.JSON(http.StatusOK, "no inefficient servers found")
			return
		}
	}

	response := models.Response{HostName: inactiveHost}

	ctx.JSON(http.StatusOK, response)
}
