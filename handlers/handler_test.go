package handlers

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"github.com/harshit-dhawan/mta-hosting-optimizer/services"
)

func TestNew(t *testing.T) {
	ctrl := gomock.NewController(t)
	mockSvc := services.NewMockHostStatus(ctrl)

	tests := []struct {
		name string
		s    services.HostStatus
		want handler
	}{
		{"Success", mockSvc, handler{svc: mockSvc}},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := New(tt.s)
			assert.Equalf(t, got, tt.want, "TEST- %s, failed", tt.name)
		})
	}
}

func GetTestGinContext(method string) (*gin.Context, *httptest.ResponseRecorder) {
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request = &http.Request{
		Header: make(http.Header),
	}

	ctx.Request.Method = method
	ctx.Request.Header.Set("Content-Type", "application/json")

	return ctx, w
}

func Test_handler_InactiveHost(t *testing.T) {
	ctrl := gomock.NewController(t)
	mockSvc := services.NewMockHostStatus(ctrl)

	mockOutput := []string{"mta-prod-1", "mta-prod-3"}
	op1 := map[string]interface{}{"HostName": []interface{}{"mta-prod-1", "mta-prod-3"}}

	err2 := errors.New("no data found from mock IP config")

	errorOutput := map[string]any{"Error": err2.Error()}

	blankMockReturn := []string{}
	errorInefficientServer := "no inefficient servers found"

	tests := []struct {
		name       string
		mockSvc    *gomock.Call
		wantStatus int
		wantBody   interface{}
	}{
		{"No inefficient server",
			mockSvc.EXPECT().InactiveHosts(gomock.Any()).Return(blankMockReturn, nil).MaxTimes(1),
			http.StatusOK, errorInefficientServer},
		{"Failure from IP config mock data",
			mockSvc.EXPECT().InactiveHosts(gomock.Any()).Return(nil, err2).MaxTimes(1),
			http.StatusBadRequest, errorOutput},
		{"Success",
			mockSvc.EXPECT().InactiveHosts(gomock.Any()).Return(mockOutput, nil).MaxTimes(1),
			http.StatusOK, op1},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := New(mockSvc)

			ctx, w := GetTestGinContext(http.MethodGet)
			h.InactiveHost(ctx)

			var got interface{}

			gotBytes, _ := io.ReadAll(w.Body)
			_ = json.Unmarshal(gotBytes, &got)

			assert.Equalf(t, tt.wantStatus, w.Code, "TEST - %s, failed", tt.name)
			assert.Equalf(t, tt.wantBody, got, "TEST - %s, failed", tt.name)
		})
	}
}
