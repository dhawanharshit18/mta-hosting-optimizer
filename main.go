package main

import (
	"log"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"

	"github.com/harshit-dhawan/mta-hosting-optimizer/handlers"
	"github.com/harshit-dhawan/mta-hosting-optimizer/mockipconfig"
	"github.com/harshit-dhawan/mta-hosting-optimizer/services"
)

const (
	base    = 10
	bitSize = 64
)

func main() {
	app := NewGinApp()

	port := GetEnvOrDefault("HTTP_PORT", "8000")
	x := GetEnvOrDefault("THRESHOLD_VALUE", "1")

	threshold, err := strconv.ParseInt(x, base, bitSize)
	if err != nil {
		log.Println("incorrect threshold value given")
		return
	}

	mockIPConfigSvc := mockipconfig.New()
	svc := services.New(threshold, mockIPConfigSvc)
	handler := handlers.New(svc)

	app.GET("/inactive-hosts", handler.InactiveHost)

	err = app.Run(":" + port)
	if err != nil {
		log.Println("Unable to start the server at Port: ", port)
		return
	}
}

func GetEnvOrDefault(k, d string) string {
	got, flag := os.LookupEnv(k)
	if !flag || got == "" {
		return d
	}

	return got
}

func NewGinApp() *gin.Engine {
	err := godotenv.Load("configs/.env")
	if err != nil {
		log.Println("failed to read env")
		return nil
	}

	// perform other initialization steps

	app := gin.Default()

	return app
}
