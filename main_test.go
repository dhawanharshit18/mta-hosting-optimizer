package main

import (
	"bytes"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestGetEnvOrDefault(t *testing.T) {
	type args struct {
		k string
		d string
	}

	tests := []struct {
		name string
		args args
		want string
	}{
		{"Success", args{k: "THRESHOLD_VALUE", d: "1"}, "1"},
		{"Key not present get default value", args{k: "ABSENT_PORT_KEY", d: "8000"}, "8000"},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := GetEnvOrDefault(tt.args.k, tt.args.d)
			assert.Equalf(t, tt.want, got, "TEST - %s failed", tt.name)
		})
	}
}

func Test_Integration(t *testing.T) {
	gin.SetMode("test")

	go main()

	tcs := []struct {
		name       string
		method     string
		endpoint   string
		body       []byte
		statusCode int
	}{

		{"success get", http.MethodGet, "/inactive-hosts", nil, http.StatusOK},
		{"failure url not found", http.MethodGet, "/wrong-url", nil, http.StatusNotFound},
	}
	for _, tc := range tcs {
		req, _ := http.NewRequest(tc.method, "http://localhost:8000"+tc.endpoint, bytes.NewBuffer(tc.body))

		c := http.Client{}

		resp, err := c.Do(req)
		if err != nil {
			t.Error(err)
		}

		if resp != nil && resp.StatusCode != tc.statusCode {
			t.Errorf("TEST - %s, failed.\tExpected %v\tGot %v\n", tc.name, tc.statusCode, resp.StatusCode)

			_ = resp.Body.Close()
		}
	}
}
