package mockipconfig

import "github.com/harshit-dhawan/mta-hosting-optimizer/models"

type IPConfigSvc struct {
}

func New() IPConfigSvc {
	return IPConfigSvc{}
}

func (i IPConfigSvc) GetIPMockData() []models.IPConfig {
	var v []models.IPConfig

	v = append(v,
		models.IPConfig{
			IP:       "127.0.0.1",
			HostName: "mta-prod-1",
			Active:   true,
		},
		models.IPConfig{
			IP:       "127.0.0.2",
			HostName: "mta-prod-1",
			Active:   false,
		},
		models.IPConfig{
			IP:       "127.0.0.3",
			HostName: "mta-prod-2",
			Active:   true,
		},
		models.IPConfig{
			IP:       "127.0.0.4",
			HostName: "mta-prod-2",
			Active:   true,
		},
		models.IPConfig{
			IP:       "127.0.0.5",
			HostName: "mta-prod-2",
			Active:   false,
		},
		models.IPConfig{
			IP:       "127.0.0.6",
			HostName: "mta-prod-3",
			Active:   false,
		})

	return v
}
