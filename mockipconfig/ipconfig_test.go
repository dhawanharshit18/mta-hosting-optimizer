package mockipconfig

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/harshit-dhawan/mta-hosting-optimizer/models"
)

func TestIPConfigSvc_GetIPMockData(t *testing.T) {
	output := initMockValue()

	tests := []struct {
		name string
		want []models.IPConfig
	}{
		{"Success", output},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			I := IPConfigSvc{}
			got := I.GetIPMockData()
			assert.EqualValuesf(t, got, tt.want, "TEST- %s, failed", tt.name)
		})
	}
}

func TestNew(t *testing.T) {
	tests := []struct {
		name string
		want IPConfigSvc
	}{
		{"Success case", IPConfigSvc{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := New(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

func initMockValue() []models.IPConfig {
	var v []models.IPConfig

	v = append(v,
		models.IPConfig{
			IP:       "127.0.0.1",
			HostName: "mta-prod-1",
			Active:   true,
		},
		models.IPConfig{
			IP:       "127.0.0.2",
			HostName: "mta-prod-1",
			Active:   false,
		},
		models.IPConfig{
			IP:       "127.0.0.3",
			HostName: "mta-prod-2",
			Active:   true,
		},
		models.IPConfig{
			IP:       "127.0.0.4",
			HostName: "mta-prod-2",
			Active:   true,
		},
		models.IPConfig{
			IP:       "127.0.0.5",
			HostName: "mta-prod-2",
			Active:   false,
		},
		models.IPConfig{
			IP:       "127.0.0.6",
			HostName: "mta-prod-3",
			Active:   false,
		})

	return v
}
