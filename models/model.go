package models

type IPConfig struct {
	IP       string `json:"IP"`
	HostName string `json:"HostName"`
	Active   bool   `json:"Active"`
}

type Response struct {
	HostName any `json:"HostName"`
}
