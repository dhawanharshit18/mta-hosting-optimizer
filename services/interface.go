package services

import (
	"github.com/gin-gonic/gin"

	"github.com/harshit-dhawan/mta-hosting-optimizer/models"
)

//go:generate mockgen -destination=mock_interfaces.go -package=services -source=interface.go

type HostStatus interface {
	InactiveHosts(ctx *gin.Context) (interface{}, error)
}

type IPConfigSvcMock interface {
	GetIPMockData() []models.IPConfig
}
