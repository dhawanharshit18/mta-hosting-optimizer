package services

import (
	"errors"

	"github.com/gin-gonic/gin"

	"github.com/harshit-dhawan/mta-hosting-optimizer/models"
)

type service struct {
	xThreshold int64
	mockConfig IPConfigSvcMock
}

//nolint:revive //service has to be unexported
func New(x int64, mockIP IPConfigSvcMock) service {
	return service{xThreshold: x, mockConfig: mockIP}
}

func (s service) InactiveHosts(ctx *gin.Context) (interface{}, error) {
	ipConfigData := s.mockConfig.GetIPMockData()
	if len(ipConfigData) == 0 {
		return nil, errors.New("no ip data found")
	}

	res := processConfigData(ipConfigData, s.xThreshold)

	return res, nil
}

func processConfigData(config []models.IPConfig, x int64) []string {
	mpHostActiveCount := map[string]int64{}

	for _, iq := range config {
		if iq.Active {
			c := mpHostActiveCount[iq.HostName]
			c++
			mpHostActiveCount[iq.HostName] = c
		} else {
			c := mpHostActiveCount[iq.HostName]
			mpHostActiveCount[iq.HostName] = c
		}
	}

	var res []string

	for s, i := range mpHostActiveCount {
		if i <= x {
			res = append(res, s)
		}
	}

	return res
}
