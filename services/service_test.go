package services

import (
	"errors"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"github.com/harshit-dhawan/mta-hosting-optimizer/models"
)

func Test_processConfigData(t *testing.T) {
	type args struct {
		config []models.IPConfig
		x      int64
	}

	tests := []struct {
		name string
		args args
		want []string
	}{
		{},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := processConfigData(tt.args.config, tt.args.x); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("processConfigData() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_service_InactiveHosts(t *testing.T) {
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	ctrl := gomock.NewController(t)
	mock := NewMockMockIPConfigSvc(ctrl)

	op1 := []string{"mta-prod-1", "mta-prod-3"}

	vv := initMockValue()

	tests := []struct {
		name       string
		xThreshold int64
		want       interface{}
		wantErr    error
		mock       *gomock.Call
	}{
		{"Success", 1, op1, nil,
			mock.EXPECT().GetIPMockData().Return(vv).Times(1)},
		{"No data found from ip mock", 1, nil, errors.New("no ip data found"),
			mock.EXPECT().GetIPMockData().Return([]models.IPConfig{}).Times(1)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(tt.xThreshold, mock)
			got, err := s.InactiveHosts(ctx)
			assert.Subsetf(t, got, tt.want, "TEST - %s, failed", tt.name)
			assert.Equalf(t, err, tt.wantErr, "TEST - %s, failed", tt.name)
		})
	}
}

func initMockValue() []models.IPConfig {
	var v []models.IPConfig

	v = append(v,
		models.IPConfig{
			IP:       "127.0.0.1",
			HostName: "mta-prod-1",
			Active:   true,
		},
		models.IPConfig{
			IP:       "127.0.0.2",
			HostName: "mta-prod-1",
			Active:   false,
		},
		models.IPConfig{
			IP:       "127.0.0.3",
			HostName: "mta-prod-2",
			Active:   true,
		},
		models.IPConfig{
			IP:       "127.0.0.4",
			HostName: "mta-prod-2",
			Active:   true,
		},
		models.IPConfig{
			IP:       "127.0.0.5",
			HostName: "mta-prod-2",
			Active:   false,
		},
		models.IPConfig{
			IP:       "127.0.0.6",
			HostName: "mta-prod-3",
			Active:   false,
		})

	return v
}
